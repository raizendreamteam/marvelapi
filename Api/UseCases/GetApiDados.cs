using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Repository.data;

namespace Api.UseCases
{

    [ApiController]
    [Route("[controller]")]
    public class GetApiDados : Controller
    {

        private readonly MarvelApiContext _marvelapicontext;

        // private readonly GetApiDados _getApiDados;

        public GetApiDados(MarvelApiContext marvelApiContext)
        {
            _marvelapicontext = marvelApiContext;
            // _getApiDados = getApiDados;
        }
       

        [HttpGet]
        public ActionResult<Personagem> GetPersonagem(
            //Passar o nome do personagem pelo body
            [FromServices]IConfiguration config,[FromBody] NomeViewModel nome)
        {
               config.GetSection("MarvelComicsAPI:Name").Value = nome.Nome;

                Personagem personagem;

                string ts = DateTime.Now.ToString();
                string publicKey = config.GetSection("MarvelComicsAPI:PublicKey").Value;
                string hash = GerarHash(ts, publicKey,
                    config.GetSection("MarvelComicsAPI:PrivateKey").Value);

                string conteudo = GerarConteudo(config,ts,publicKey,hash);

                dynamic resultado = JsonConvert.DeserializeObject(conteudo);

               string va = config.GetSection("MarvelComicsAPI:BaseURL").Value +
                    $"characters?ts={ts}&apikey={publicKey}&hash={hash}&" +
                    $"name={Uri.EscapeUriString(config.GetSection("MarvelComicsAPI:Name").Value)}";

                personagem = new Personagem();
                personagem.Nome = resultado.data.results[0].name;
                personagem.Descricao = resultado.data.results[0].description;
                personagem.UrlImagem = resultado.data.results[0].thumbnail.path + "." +
                    resultado.data.results[0].thumbnail.extension;
                personagem.UrlWiki = resultado.data.results[0].urls[1].url;
                _marvelapicontext.Personagens.Add(personagem);
                _marvelapicontext.SaveChanges();

            return Ok(va);
        }

        private string GerarConteudo(IConfiguration config,string ts, string publicKey, string hash)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));


                HttpResponseMessage response = client.GetAsync(
                    config.GetSection("MarvelComicsAPI:BaseURL").Value +
                    $"characters?ts={ts}&apikey={publicKey}&hash={hash}&" +
                    $"name={Uri.EscapeUriString(config.GetSection("MarvelComicsAPI:Name").Value)}").Result;

                response.EnsureSuccessStatusCode();
                string conteudo = response.Content.ReadAsStringAsync().Result;
                return conteudo;
            }
        } 
        private string GerarHash(
                 string ts, string publicKey, string privateKey)
        {
            byte[] bytes =
                    Encoding.UTF8.GetBytes(ts + privateKey + publicKey);
            var gerador = MD5.Create();
            byte[] bytesHash = gerador.ComputeHash(bytes);
            return BitConverter.ToString(bytesHash)
                .ToLower().Replace("-", String.Empty);
        }
    }
}

   
