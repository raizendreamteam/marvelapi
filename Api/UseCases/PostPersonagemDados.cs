
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Repository.data;

namespace Api.UseCases
{
    [ApiController]
    [Route("[controller]")]
    public class PostPersonagemDados : ControllerBase
    {

        private readonly MarvelApiContext _marvelapicontext;

        private GetApiDados _getApiDados;

        Personagem personagemApi = new Personagem();

        public PostPersonagemDados(MarvelApiContext marvelApiContext, GetApiDados getApiDados, [FromServices] IConfiguration config)
        {
            _marvelapicontext = marvelApiContext;
            _getApiDados = getApiDados;
        }


        
            

        [HttpPost]
        public ActionResult<Personagem> InsertDadosPersonagens()
        {
            
            var personagem = new Personagem
            {
                Nome = "homiaranha",
                Descricao = "solta teia",
                UrlImagem = "nao tem",
                UrlWiki = "wikipedia"
            };

            if (string.IsNullOrWhiteSpace(personagemApi.Nome))
            {
                return BadRequest(new{
                    Error = "Campo nome obrigatório"
                });
            }

            _marvelapicontext.Personagens.Add(personagemApi);
            _marvelapicontext.SaveChanges();

            return Ok(personagemApi);
        }
    }
}