using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Api.UseCases
{
    [ApiController]
    [Route("[controller]")]
    public class GetPersonagemDados : ControllerBase
    {
        [HttpGet]
        public ActionResult<Personagem> GetByName(string nomePersonagem)
        {
            var personagem = new Personagem();
            return Ok(personagem);
        }
    }
}