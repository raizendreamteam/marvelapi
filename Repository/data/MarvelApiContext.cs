using Domain.Entities;
using Microsoft.EntityFrameworkCore;



namespace Repository.data
{
    public class MarvelApiContext : DbContext
    {
        public MarvelApiContext (DbContextOptions<MarvelApiContext> options) : base (options) { }
        
        public virtual DbSet<Personagem> Personagens { get; set; }
    }    
}